# GitLab CI/CD Template for Salesforce delta Deployment

This project contains a fully configured CI pipeline that works with Salesforce DX projects. There are two variation of pipeline:
1. Org Based ALM Pipeline
2. Package Based ALM Pipeline

Pipelines mentioned above has different use cases. [Learn](https://trailhead.salesforce.com/content/learn/modules/application-lifecycle-and-development-models) which ALM is suitable for your needs.

# Setup GitLab CI Runner with Docker Executor or Shared Runner
**Note:** Below step is not required if you want to use Default Gitlab Shared Runner for pipelines.

As of today(July 2022), Grazitti GitLab CI Runner run over shell executor. To install all the important dependencies, its recommended to use docker executor. By default everything runs over shared Gitlab runners. We can easily setup our own runner(docker based) by following help [link](https://medium.com/devops-with-valentine/setup-gitlab-ci-runner-with-docker-executor-on-windows-10-11-c58dafba9191).

**Note:** Above step is not required if you want to use Default Gitlab Shared Runner for pipelines.


# Org Based pipelines 

### Initial Project Setup(by Owner)

1. Create a GIT project. Add users into it with appropriate role.
2. Create a Salesforce DX project and export all the necessary metadata.
3. Add a new file to project with name **.gitlab-ci.yml** having following content:
```yml
include:
  - project: 'manish_kumar/sfdc-cicd-template'
    ref: master
    file: '/org-based.gitlab-ci.yml'
```
4. Add a js file to the project with name **parsePR.js**. Copy it's content from CI/CD project file(with same name).
**Note:** Above step is required. Do check the file name as it should be the same as in CICD Template Project.
5. Initialize git and push the changes to master branch.
6. Create a branch **dev** by cloning it from master. This branch will point to Integration Step in pipeline.
7. Make both master and dev branches as protected So no one could push changes directly into it. Every incoming change must go through merge request (This step is up to you, how you want to process).
8. Configure Environment Variables. **These are the global variables that will help the pipelines to authenticate and below mentioned variable must be added with the same name.**

Here are the Auth URL variables to set:

- `DEV_AUTH_URL`
- `DEV_LOGIN_URL`
- `PROD_AUTH_URL`
- `PROD_LOGIN_URL`

### Configure Environment Variables

1. Install [Salesforce CLI](https://developer.salesforce.com/tools/sfdxcli) on your local machine.
2. [Authenticate using the web-based oauth flow](https://developer.salesforce.com/docs/atlas.en-us.sfdx_dev.meta/sfdx_dev/sfdx_dev_auth_web_flow.htm) to each of the orgs that represent your Dev Hub, sandbox, and production org. 
3. Use the `sfdx force:org:display --targetusername <username> --verbose` command to get the [Sfdx Auth Url](https://developer.salesforce.com/docs/atlas.en-us.sfdx_dev.meta/sfdx_dev/sfdx_dev_auth_view_info.htm) for your various orgs. The URL you're looking for starts with `force://`. **Note, you must use the `--verbose` argument to see the Sfdx Auth URL.**
4. Populate the variables under **Settings > CI / CD > Variables** in your project in GitLab.



### Process Guidelines(End-to-End)

1. **Developer** clone the project git repository in a local machine.
2. Create a feature branch to work on specific task/project. 
**Note:** try to follow naming pattern as *feature/{taskname}*.
3. Make the required changes and push it to developer box followed by unit testing.
4. If everything looks good, push the changes to remote repository.
**Note:** By default Pipeline will set a default test level. In case we want to run specific apex test, add following string in commit message:
**Apex::[TestClassName1,TestClassName2]::Apex**   
Example:- While commiting changes use commit command like **git commit -m "New Changes Added Apex::[acctrigHandlerTest,acctrigHandlerTest2]::Apex**"
5. Go to Gitlab and create a merge request to merge changes from feature branch to Develop branch.
6. This merge request will invoke the CI pipeline automatically.
![CI Pipeline](https://git.grazitti.com/aman-malik/sfdc-cicd-template/uploads/08cef369c9b225d13e9cae6baabf525c/CI.png)
7. **Project Owner** review the merge request and check pipeline(manifest, test results and code scanner results).
8. In case any update is needed, Owner ask the Developer to make the changes.
9. When merge request is good to go, Owner deploy the changes to integration box by clicking on Integration button under pipeline. Moreover Owner merge the changes to Develop branch.
10. QA person test the changes in Integration box.
11. If all goes well, Owner make a merge request to move changes from Develop to master branch. This step again invoke a CD pipeline.
12. If pipeline test run successfully, Owner deploy the changes to full and production instances followed by merging changes to master branch.
